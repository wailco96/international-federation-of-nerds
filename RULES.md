# Contents
- [Community Rules](#community-rules)
  1. [General rules](#1-general-rules)
  2. [Channel guidelines](#2-channel-guidelines)
- [Abbreviation Explained](#abbreviation-explained)
- [Extra Resources](#extra-resources)

# Community Rules
### 1. General rules
  1. Be respectful to staff and fellow members of the community.
  2. No offensive messages, nicknames or any other way.
  3. No harassment (including sexual harassment or encouraging of harassment).
  4. Try to avoid drama, keep the chat PC* and friendly, do not join the drama either.
  5. Keep topic related chats in the intended channels.
  6. No self-promotion or advertising of discord servers without permission.
  7. No NSFW content of any kind, keep the chat non-adult friendly.
  8. Do not spam uppercases, emojis, line breaks or any other kind of spam.
  9. No self-botting, user-bots or imitating to be a bot.
  10. Use common sense. If you think you'll get a warning for it, don't do it.
  11. Don't discuss religion, anything illegal or anything NSFW.
  12. Political discussions are allowed but try to avoid these, they tend to bring lots of drama.
  13. Follow and respect the Discord ToS* and Guidelines!

### 2. Channel guidelines
  1. **Offtopic**
      1. Here can non-topic related conversation happen.
      2. Images can be shared here, but only share images if the images are fitting the current conversation topic.
      3. Same goes for url/link, only share them when it fits the current conversation topic.
      4. Try to contruct your message clearly, before the whole channel gets floaded by messages from one user.
  2. **Pictures Sharing**
      1. If the picture you wanna share has their own corresponding channel, use that.
      2. If it has no topic use the `#others` channel.
      3. Do not overshare picutres, we will handle this as spam.
      4. No NSFW content of any kind, keep the chat non-adult friendly.
  3. **School Nerds**
      1. Only school/study/education related conversations can be hold here.
      2. Picture sharing is allowed, but only if it has something to do with the topic.

# Abbreviation Explained
- **PC**: Politically Correct
- **ToS**: Terms of Service

# Extra Resources
- [Discord's Terms of Service](https://discord.com/terms)
- [Discord's Community Guidelines](https://discord.com/guidelines)